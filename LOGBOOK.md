# Team R Work Logbook

| Date     | Activity                                       | Sam       | Coenraad  | Brett     |
| -------- | ---------------------------------------------- | --------- | --------- | --------- |
| 18/05/21 | Initial meeting to discuss requirements        | 2h        | 2h        | 2h        |
| 21/05/21 | Set up BitBucket. <BR>Created initial .ino files for each device. <BR>Created initial library for protocol <BR>Created initial worklog template <BR>Created initial report template                |           |           | 1h        |
| 24/05/21 | Created updated report template <BR> Commit: 1eb4bcf |           | 1h        |           |
| 24/05/21 | Removed library due to incompatibility with simulator. <BR> Started to work on speed/fuel slave.Protocol RX functional, calculation of frequency of RPM   and speed working. <BR> Commit: 115cb52b |           |           | 2h        |
| 25/05/21 | Meeting to discuss information for distance, speed and fuel. <BR>Allocation of controllers. Brett Francis for Speed & Fuel. Sam for Display. Coenraad for Distance. <BR>Commit     2d1805ea      |  5h       |  5h       |  5h       |
| 25/05/21 | Added information on distance, speed and fuel. including calculations. Commit     8a4ec9ca    |           |  3h         |          |
| 26/05/21 | Basic code for distance sensors added.    Commit:    8442a3db   |           |  1h         |           |
| 28/05/21 | Speed/Fuel Slave:  Fixed issue with checksum calculation.<BR> Fixed issue with receiving message terminated in wrong place. <BR> Started to implement message parsing. <BR> Commit: 4591d712 |           |           | 3h        |
| 28/05/21 | Updated report, draft of Protocol functional specification. <BR> Commit: 74baee08 |           |           | 1h         |
| 30/05/21 | Speed/Fuel Slave: Implemented full transmit and receive functionality.  <BR> Fixed calculation of fuel economy. <BR> Commit: f13f7b15 |           |          | 2h        |
| 01/06/21 | Code given to Brett and Coenraad from Sam for initial Display implementation with Distance, Speed and Fuel for team review. <BR>Reformat and begin report in MS Word, discussion over display flow concept  chart. <BR>Commit: 2d1805ea |   7h      |  3h      |   3h      |
| 30/05/21 | Started to implement protocol in distance sensor slave. <BR>  Commit: b529664ad |           |           | 1h        |
| 30/05/21 | Finished distance sensor slave.  General Bug fix on Speed Fuel.  Commit: 3e97226d |           |           | 2h        |
| 01/06/21 | Drawn flow charts for protocol and parameters in autoCAD  Commit: 1d8cd672 | 5h        |           |           |
| 01/06/21 | Modify flow charts and complete display chart in autoCAD  Commit: 2d1805ea | 3h        |           |           |
| 02/06/21 | Modify word document to include mark up components. Commit: 970c6642| |2h||
| 04/06/21 | Completed testing on distance sensor. Updated word document||2h||
| 04/06/21 | Updated word document with animation figures and tabled data Commit: 2d1805ea |           | 3h        |           |
| 05/06/21 | Power Point Development by Coenraad for review by Sam and Brett. Commit: 7913cc63 |  1h       | 5h        |  1h        |
| 05/06/21 | Modification of Word Document and Generation of Testing Tables. Commit: e43d34af |  4h       | 4h        |  4h        |
| 05/06/21 | Presentation Video. Commit: e43d34af |  1h       | 1h        |  1h        |
| 05/06/21 | Bugfix of protocol to reduce memory usage. Commit: 9db9d6ce |           |           |  0.5h     |
| 05/06/21 | Updated word document protocol specification.  Included common terms and defined sentences.  Commit: 5e5e3f74 |           |           |  1.5h     |
| 05/06/21 | Updated word document attached ino files, reviewed changes  Commit: fdcac477 |  1h         |  1h        |           |
||**Running total**                             | **29.0h** | **29.0h** | **30.0h** |