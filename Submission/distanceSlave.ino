//Include Libraries
#include <SoftwareSerial.h>
#include <String.h>

//Simulating in Tinkercad?
//#define _tinkercad

//Display debug messages?
#define _DEBUGMODE

//Pin Assignments
#define COM_RX 2
#define COM_TX 3
#define COM_EN 11
#define DIO_SENSOR0 5
#define DIO_SENSOR1 6
#define DIO_SENSOR2 7
#define DIO_SENSOR3 8
#define DO_LED 13

//Define Message
#define slaveAddress "SSRD" //SS = Slave Sensor, RD = Rear Distance

// Protocol Constants
#define START 0x24 //Start of application message, <$>
#ifdef _tinkercad //different for tinkercad simulation
  #define ESCAPE 0x25  //End of application message, <%>
#else
  #define ESCAPE 0x0D  //End of application message, <CR>
#endif
#define DELIM_PARAM "," //Delimiter of payload parameters <,>
#define DELIM_CHECKSUM 0x2A //Delimiter payload and checksum <*>

// Protocol Specfic Length
#define PAYLOAD_MAX_LENGTH 100 //100 bytes (100 characters in payload, 110 total) 
#define PARAM_MAX_LENGTH 15 //bytes
#define SP_TYPE_LENGTH 4 //Application message type length
#define SP_CHECKSUM_LENGTH 2 //Checksum length
#define BUFFER_SIZE (PAYLOAD_MAX_LENGTH+SP_TYPE_LENGTH+2) //Size of payload + type + delim + null termination
#define SP_MAX_PARAMS 10 //Max number of paramters

//Message RX Status
#define MESSAGE_RX_IDLE 0
#define MESSAGE_RX_INPROGRESS 1
#define MESSAGE_RX_READY 2

// Protocol Status Constants
#define SUCCESS 0
#define NO_ERROR 0
#define CHECKSUM_FAILURE 1
#define PAYLOAD_EXCEEDS_LENGTH 2
#define TYPE_EXCEEDS_LENGTH 3
#define START_BYTE_UNEXPECTED 4
#define UNKNOWN_MESSAGE_TYPE 5

//PRECISION
#define FLOAT_DPOINTS 2 //Digits after decimal point float.

//Sensor Constants
#define SPEED_SOUND_AIR_CONSTANT 331.5 //METRES PER SECOND
#define AMBIENT_TEMPERATURE 24 //DEG C

#define NUM_SENSORS 4
#define DISTANCE0 0 //Sensor 0
#define DISTANCE1 1 //Sensor 0
#define DISTANCE2 2 //Sensor 0
#define DISTANCE3 3 //Sensor 0
#define PULSE_TIMEOUT 20 // 20ms

// Update Rate
#define UPDATE_PERIOD 200 //ms


//Start Serial Port
SoftwareSerial mySerial(COM_RX, COM_TX);

//Serial Port RX/TX
char rxMessageBuffer[BUFFER_SIZE]; //Buffer for rx recieved
char txMessageBuffer[PAYLOAD_MAX_LENGTH]; //Buffer for tx
char messageChecksum[SP_CHECKSUM_LENGTH+1]; //Plus string terminator
int messageStatus = MESSAGE_RX_IDLE;
int charCount = 0;
bool calculateChecksum = true;
int myChecksum = 0;
int byteRead = 0;
int sysError = NO_ERROR;
char rxMessageParamter[SP_MAX_PARAMS][PARAM_MAX_LENGTH]; //Parameters
char rxMessageType[SP_TYPE_LENGTH+2];
char *sptr;

//Internal Variables
int sensorPins[] = {DIO_SENSOR0, DIO_SENSOR1, DIO_SENSOR2, DIO_SENSOR3};
long sensorDistance[NUM_SENSORS];
long sensorDistanceMin;
int currentSensor = DISTANCE0;
long sensorTimer0;
long sensorTimer1;

//Setup Program
void setup()
{
  Serial.begin(9600);
  mySerial.begin(2400);
  
  #ifdef _DEBUGMODE
    Serial.println("Starting..");
  #endif

  //start update timer
  sensorTimer0 = millis();

  //Force TX pin high
  digitalWrite(COM_TX,HIGH);
}

void loop()
{
  //Check serial port for data
  //myserial
  if (Serial.available() > 0 && sysError <= NO_ERROR && (messageStatus == MESSAGE_RX_IDLE || messageStatus == MESSAGE_RX_INPROGRESS))
  {
    //read byte
    //myserial
    byteRead = Serial.read();

    //Make sure we're not too long
    if (charCount > BUFFER_SIZE)
    {
      sysError = PAYLOAD_EXCEEDS_LENGTH;
    }
    else
    {
      //check if we're already recieving a message
      if (messageStatus == MESSAGE_RX_INPROGRESS)
      {
        switch(byteRead)
        {
          case START: //Unexpected START char in message
            sysError = START_BYTE_UNEXPECTED;
            break;
          case ESCAPE: //End of Transmission
            //Check checksum is matches what has been calculated
            #ifdef _DEBUGMODE
              Serial.println(myChecksum);
              Serial.println(strtol(messageChecksum, 0, 16));
            #endif
            //Convert string to hex (base 16)
            if (strtol(messageChecksum, 0, 16) == myChecksum)
            {
              messageStatus = MESSAGE_RX_READY;
              sysError = SUCCESS;
            }
            else
            {
              sysError = CHECKSUM_FAILURE;
            }
            break;
          case DELIM_CHECKSUM: //Checksum is getting transmitted
            calculateChecksum = false;
            //convert char array to string
            rxMessageBuffer[charCount] = '\0';
            //Reset char count
            charCount = 0;
            break;
          default:
            if (calculateChecksum  == true)
            {
              myChecksum ^= byteRead; //XOR
              rxMessageBuffer[charCount] = byteRead;
            }
            else
            {
              //Recieving Checksum
              messageChecksum[charCount] = byteRead;
            }
            charCount++;
        }
      }
      else //Not receiving a message currently, first character!
      {
        resetEverything();

        //Make sure we are receiving the START character
        if (byteRead == START)
        {
          messageStatus = MESSAGE_RX_INPROGRESS;
        }//else ignore it!
      }
    }
  }

  //Check if we've encountered an RX Error
  if (sysError > NO_ERROR)
  {
    #ifdef _DEBUGMODE
      switch(sysError)
      {
        case UNKNOWN_MESSAGE_TYPE:
          Serial.println("Unknown Message Type");
          break;
        case CHECKSUM_FAILURE:
          Serial.println("Checksum Fail");
          break;
        case PAYLOAD_EXCEEDS_LENGTH:
          Serial.println("Message too long");
          break;
        case TYPE_EXCEEDS_LENGTH:
          Serial.println("Payload too long");
          break;
        case START_BYTE_UNEXPECTED:
          Serial.println("Start byte recieved unexpectedly");
          break;
        default:
          Serial.println("Unknown error!");
          break;      
      }
    #endif
    sysError = NO_ERROR;
    resetEverything();
  }

  //Parse recieved message
  if(messageStatus == MESSAGE_RX_READY)
  {
    #ifdef _DEBUGMODE
      Serial.println("String RX OK.");
      Serial.print("Recieved ");
      Serial.print(strlen(rxMessageBuffer));
      Serial.println(" chars.");
    #endif
    
    //Isolate the first part
    sptr = strtok(rxMessageBuffer, DELIM_PARAM);

    //Move it to the type
    strcpy(rxMessageType, sptr);
    
    #ifdef _DEBUGMODE
      Serial.print("Message type '");
      Serial.print(rxMessageType);
      Serial.println("'");
    #endif
    
    //Check if we can handle this message
    if (strcmp(rxMessageType, "DDRQ") == 0)
    {
      #ifdef _DEBUGMODE
        Serial.println("Message is able to be handled");
      #endif
      processMessage();
      if (strcmp(rxMessageParamter[0], slaveAddress) == 0)
      {
        Serial.println("Slave Request Recieved");
                
        //generate buffer string
        snprintf(txMessageBuffer, PAYLOAD_MAX_LENGTH, "%lu%s%lu%s%lu%s%lu%s%lu", sensorDistance[DISTANCE0], DELIM_PARAM, sensorDistance[DISTANCE1], DELIM_PARAM, sensorDistance[DISTANCE2], DELIM_PARAM, sensorDistance[DISTANCE3], DELIM_PARAM, sensorDistanceMin);
        sendMessage(slaveAddress, txMessageBuffer);

        //Force TX pin high
        digitalWrite(COM_TX,HIGH);
      }
    }
    else {
      #ifdef _DEBUGMODE
        Serial.println("Message is unknown");
      #endif
      sysError = UNKNOWN_MESSAGE_TYPE;
    }
    resetEverything();
  }

  //Check if it's time to poll a distance sensor
  sensorTimer1 = millis();
  if ((sensorTimer1 - sensorTimer0) > UPDATE_PERIOD )
  {
    //reset timer
    sensorTimer0 = sensorTimer1;

    //Check count
    if (currentSensor > NUM_SENSORS)
    {
      currentSensor = DISTANCE0;
      sensorDistanceMin = getMinimum(sensorDistance, NUM_SENSORS);
      #ifdef _DEBUGMODE
        Serial.print("Got Minimum Distance: ");
        Serial.print(sensorDistanceMin);
        Serial.println("mm");
      #endif
    }
    
    #ifdef _DEBUGMODE
      Serial.print("Reading distance sensor #");
      Serial.print(currentSensor);
    #endif
    
    sensorDistance[currentSensor] = pulseDistanceSensor (sensorPins[currentSensor]);
    
    #ifdef _DEBUGMODE
      Serial.print(". Distance was ");
      Serial.println(sensorDistance[currentSensor]);
    #endif
    
    //Set next sensor to be polled
    currentSensor++;
    Serial.println(currentSensor);
  }
}

//long microsecondsToCentimeters(long microseconds) {
  // The speed of sound is 343 m/s , 29.154519 microseconds per centimeter.
  //return microseconds / 29.15 / 2;
//}

long getMinimum(long *distancevals, int numvalues)
{
  int minindex = 0;
  for (int i=1; i < numvalues; i++)
  {
    if(distancevals[i] < distancevals[minindex])
    {
      minindex = i;
    }
  }
  return distancevals[minindex];
}

void resetEverything()
{
  #ifdef _DEBUGMODE
    Serial.println("Reset Everything!");
  #endif
  //Reset everything
  messageStatus = MESSAGE_RX_IDLE;
  calculateChecksum = true;
  charCount = 0;
  myChecksum = 0;
  memset(rxMessageBuffer, 0, sizeof(rxMessageBuffer));//Clear RX Buffer
  memset(messageChecksum, 0, sizeof(messageChecksum));//Clear Checksum Buffer
  memset(txMessageBuffer, 0, sizeof(txMessageBuffer));//Clear TX Buffer
}

void processMessage()
{
  //Process the rest of the message
  for (int i=0; i<= SP_MAX_PARAMS-1; i++)
  {
    #ifdef _DEBUGMODE
      Serial.println(i);
    #endif
    //Get next index
    sptr = strtok(NULL, DELIM_PARAM);
    if (sptr == NULL)
    {
      break;
    }
    strcpy(rxMessageParamter[i], sptr);
    #ifdef _DEBUGMODE
      Serial.println(rxMessageParamter[i]);
    #endif
      
  }
}

void sendMessage(char *txMessageType, char *txMessageParams)
{

  #ifdef _DEBUGMODE
    Serial.println("Sending Message!");
  #endif
  //Check lengths
  if (strlen(txMessageParams) > PAYLOAD_MAX_LENGTH)
  {
    sysError = PAYLOAD_EXCEEDS_LENGTH;
    return;
  }
  
  if (strlen(txMessageType) > SP_TYPE_LENGTH)
  {
    sysError = TYPE_EXCEEDS_LENGTH;
    return;
  }
    
  //Initialise message
  char payloadMessage[BUFFER_SIZE]; //Size of type + delim + payload
  char txMessage[BUFFER_SIZE + SP_CHECKSUM_LENGTH + 3]; //Size of START + type + delim + payload + delim + checksum

  //Create string to calculate checksum
  sprintf (payloadMessage, "%s%s%s", txMessageType, DELIM_PARAM, txMessageParams);
  
  //Calculate checksum
  int txChecksum = 0;
  for (int i=0; i<=strlen(payloadMessage); i++)
  {
    txChecksum ^= payloadMessage[i];
  }

  //Formulate message to send
  sprintf (txMessage, "%c%s%c%x%c", START, payloadMessage, DELIM_CHECKSUM, txChecksum, ESCAPE);

  //Send Message
  //mySerial
  Serial.print(txMessage);
}

long pulseDistanceSensor (int DIO_PIN)
{
  Serial.println(DIO_PIN);
  
  //Setup local variables
  long duration, cm;

  //Pin to output
  pinMode(DIO_PIN, OUTPUT);

  //Ensure low
  digitalWrite(DIO_PIN, LOW);
  delayMicroseconds(2);
  //High for 5uS
  digitalWrite(DIO_PIN, HIGH);
  delayMicroseconds(5);

  //Return to low
  digitalWrite(DIO_PIN, LOW);

  //Read response
  pinMode(DIO_PIN, INPUT);

  //Wait for a pulse up to timeout
  duration = pulseIn(DIO_PIN, HIGH, PULSE_TIMEOUT);
  #ifdef _DEBUGMODE
    Serial.print(". Duration was... ");
    Serial.print(duration);
  #endif
  
  if (duration == 0)
  {
    return 9999;
  }
  else
  {
    // Speed of Sound in air is 331.5 + (0.6 x TEMP) m/s
    // m/s * 1000mm/1000000us = mm/us 
    // mm/us * us = mm
    // Distance half of round trip time (there and back)
    return duration * ((SPEED_SOUND_AIR_CONSTANT + (AMBIENT_TEMPERATURE*0.6))/10000)/2;
  }
}