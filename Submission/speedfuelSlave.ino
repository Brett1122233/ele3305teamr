//Include Libraries
#include <SoftwareSerial.h>
#include <String.h>

//Simulating in Tinkercad?
//#define _tinkercad

//Display debug messages?
#define _DEBUGMODE

//Pin Assignments
#define COM_RX 4
#define COM_TX 5
#define COM_EN 11
#define DI_FUEL 2
#define DI_SPEED 3
#define DO_LED 13

//Define Message
#define slaveAddress "SSSF" //SS = Slave Sensor, SF = Speed Fuel

// Protocol Constants
#define START 0x24 //Start of application message, <$>
#ifdef _tinkercad //different for tinkercad simulation
  #define ESCAPE 0x25  //End of application message, <%>
#else
  #define ESCAPE 0x0D  //End of application message, <CR>
#endif
#define DELIM_PARAM "," //Delimiter of payload parameters <,>
#define DELIM_CHECKSUM 0x2A //Delimiter payload and checksum <*>

// Protocol Specfic Length
#define PAYLOAD_MAX_LENGTH 100 //100 bytes (100 characters in payload, 110 total) 
#define PARAM_MAX_LENGTH 15 //bytes
#define SP_TYPE_LENGTH 4 //Application message type length
#define SP_CHECKSUM_LENGTH 2 //Checksum length
#define BUFFER_SIZE (PAYLOAD_MAX_LENGTH+SP_TYPE_LENGTH+2) //Size of payload + type + delim + null termination
#define SP_MAX_PARAMS 10 //Max number of paramters

//Message RX Status
#define MESSAGE_RX_IDLE 0
#define MESSAGE_RX_INPROGRESS 1
#define MESSAGE_RX_READY 2

// Protocol Status Constants
#define SUCCESS 0
#define NO_ERROR 0
#define CHECKSUM_FAILURE 1
#define PAYLOAD_EXCEEDS_LENGTH 2
#define TYPE_EXCEEDS_LENGTH 3
#define START_BYTE_UNEXPECTED 4
#define UNKNOWN_MESSAGE_TYPE 5

//Sensor Constants
#define WHEEL_CIRCUM 1994 //mm
#define INJECTOR_SIZE 200 //cc/min
#define CYLINDER_COUNT 4
#define SPEED_DUTY_CYCLE 0.5 //percent
#define FUEL_DUTY_CYCLE 0.5 //percent

//PRECISION
#define FLOAT_DPOINTS 2 //Digits after decimal point float.

//Start Serial Port
SoftwareSerial mySerial(COM_RX, COM_TX);

//Internal Variables
//Speed calculations
float speedCurrentVal = 0; //METRES PER SECOND
float speedFreq = 0; //HERTZ
bool speedAlreadyHIGH = false; 
int speedStartT = 0; //MILLISECONDS
int speedEndT = 0; //MILLISECONDS

//Fuel calculations
float fuelCurrentVal = 0; //L PER 100km
float fuelAverageVal = 0; //L PER 100km
float fuelFreq = 0; //HERTZ
bool fuelAlreadyHIGH = false; 
int fuelStartT = 0; //MILLISECONDS
int fuelEndT = 0; //MILLISECONDS

//Distance calculations
unsigned int distanceCurrentVal = 0; //METRES


//Serial Port RX/TX
char rxMessageBuffer[BUFFER_SIZE]; //Buffer for rx recieved
char txMessageBuffer[PAYLOAD_MAX_LENGTH]; //Buffer for tx
char messageChecksum[SP_CHECKSUM_LENGTH+1]; //Plus string terminator
int messageStatus = MESSAGE_RX_IDLE;
int charCount = 0;
bool calculateChecksum = true;
int myChecksum = 0;
int byteRead = 0;
int sysError = NO_ERROR;
char rxMessageParamter[SP_MAX_PARAMS][PARAM_MAX_LENGTH]; //Parameters
char rxMessageType[SP_TYPE_LENGTH+2];
char *sptr;
double timeSpeed0;
double timeSpeed1;
char sfuelCurrentVal[15];
char sfuelAverageVal[15];
char sspeedCurrentVal[15];

//Setup Program
void setup()
{
  Serial.begin(9600);
  mySerial.begin(2400);
  
  #ifdef _DEBUGMODE
    Serial.println("Starting..");
  #endif

  //Start distance counter
  timeSpeed0 = millis();

  //Force TX pin high
  digitalWrite(COM_TX,HIGH);
}

void loop()
{
  //Check serial port for data
  //myserial
  if (mySerial.available() > 0 && sysError <= NO_ERROR && (messageStatus == MESSAGE_RX_IDLE || messageStatus == MESSAGE_RX_INPROGRESS))
  {
    //read byte
    //myserial
    byteRead = mySerial.read();

    //Make sure we're not too long
    if (charCount > BUFFER_SIZE)
    {
      sysError = PAYLOAD_EXCEEDS_LENGTH;
    }
    else
    {
      //check if we're already recieving a message
      if (messageStatus == MESSAGE_RX_INPROGRESS)
      {
        switch(byteRead)
        {
          case START: //Unexpected START char in message
            sysError = START_BYTE_UNEXPECTED;
            break;
          case ESCAPE: //End of Transmission
            //Check checksum is matches what has been calculated
            #ifdef _DEBUGMODE
              Serial.println(myChecksum);
              Serial.println(strtol(messageChecksum, 0, 16));
            #endif
            //Convert string to hex (base 16)
            if (strtol(messageChecksum, 0, 16) == myChecksum)
            {
              messageStatus = MESSAGE_RX_READY;
              sysError = SUCCESS;
            }
            else
            {
              sysError = CHECKSUM_FAILURE;
            }
            break;
          case DELIM_CHECKSUM: //Checksum is getting transmitted
            calculateChecksum = false;
            //convert char array to string
            rxMessageBuffer[charCount] = '\0';
            //Reset char count
            charCount = 0;
            break;
          default:
            if (calculateChecksum  == true)
            {
              myChecksum ^= byteRead; //XOR
              rxMessageBuffer[charCount] = byteRead;
            }
            else
            {
              //Recieving Checksum
              messageChecksum[charCount] = byteRead;
            }
            charCount++;
        }
      }
      else //Not receiving a message currently, first character!
      {
        resetEverything();

        //Make sure we are receiving the START character
        if (byteRead == START)
        {
          messageStatus = MESSAGE_RX_INPROGRESS;
        }//else ignore it!
      }
    }
  }

  //Check if we've encountered an RX Error
  if (sysError > NO_ERROR)
  {
    #ifdef _DEBUGMODE
      switch(sysError)
      {
        case UNKNOWN_MESSAGE_TYPE:
          Serial.println("Unknown Message Type");
          break;
        case CHECKSUM_FAILURE:
          Serial.println("Checksum Fail");
          break;
        case PAYLOAD_EXCEEDS_LENGTH:
          Serial.println("Message too long");
          break;
        case TYPE_EXCEEDS_LENGTH:
          Serial.println("Payload too long");
          break;
        case START_BYTE_UNEXPECTED:
          Serial.println("Start byte recieved unexpectedly");
          break;
        default:
          Serial.println("Unknown error!");
          break;      
      }
    #endif
    sysError = NO_ERROR;
    resetEverything();
  }

  //Parse recieved message
  if(messageStatus == MESSAGE_RX_READY)
  {
    #ifdef _DEBUGMODE
      Serial.println("String RX OK.");
      Serial.print("Recieved ");
      Serial.print(strlen(rxMessageBuffer));
      Serial.println(" chars.");
    #endif
    
    //Isolate the first part
    sptr = strtok(rxMessageBuffer, DELIM_PARAM);

    //Move it to the type
    strcpy(rxMessageType, sptr);
    
    #ifdef _DEBUGMODE
      Serial.print("Message type '");
      Serial.print(rxMessageType);
      Serial.println("'");
    #endif
    
    //Check if we can handle this message
    if (strcmp(rxMessageType, "DDRQ") == 0)
    {
      #ifdef _DEBUGMODE
        Serial.println("Message is able to be handled");
      #endif
      processMessage();
      if (strcmp(rxMessageParamter[0], slaveAddress) == 0)
      {
        Serial.println("Slave Request Recieved");
        
        //get around problem with lack of support for floats by converting them to strings first
        dtostrf(fuelCurrentVal, 0, FLOAT_DPOINTS, sfuelCurrentVal);
        dtostrf(fuelAverageVal, 0, FLOAT_DPOINTS, sfuelAverageVal);
        dtostrf(speedCurrentVal, 0, FLOAT_DPOINTS, sspeedCurrentVal);
        
        //generate buffer string
        snprintf(txMessageBuffer, PAYLOAD_MAX_LENGTH, "%s%s%s%s%s%s%i", sfuelCurrentVal, DELIM_PARAM, sfuelAverageVal, DELIM_PARAM, sspeedCurrentVal, DELIM_PARAM, distanceCurrentVal);
        sendMessage(slaveAddress, txMessageBuffer);

        //Force TX pin high
        digitalWrite(COM_TX,HIGH);
      }
    }
    else {
      #ifdef _DEBUGMODE
        Serial.println("Message is unknown");
      #endif
      sysError = UNKNOWN_MESSAGE_TYPE;
    }
    resetEverything();
  }

  //Start frequency counter code
  //Speed Sensor
  if (digitalRead(DI_SPEED) == HIGH)
  {
    //Serial.println("HIGH");
    
    if (speedAlreadyHIGH == false) //Transitioned from LOW to HIGH, start counter
    {
      speedStartT = millis();
      speedAlreadyHIGH = true;
    }
  }
  else //LOW
  {
    //Serial.println("LOW");
    //Transitioned from HIGH to LOW, end counter
    if (speedAlreadyHIGH == true) 
    {
      //get next counter
      speedEndT = millis();
      speedAlreadyHIGH = false;
      speedFreq = (1000/((speedEndT - speedStartT)/SPEED_DUTY_CYCLE)); //
      //Serial.print("Current Speed: ");
      //Serial.println(speedFreq, DEC);
      speedCurrentVal = WHEEL_CIRCUM*speedFreq; //METRES * 1/SECOND = METRES PER SECOND

      //get time since last speed measurement
      timeSpeed1 = millis();
      
      //m/s * 1000ms = m
      distanceCurrentVal = speedCurrentVal * 1000 * (timeSpeed1-timeSpeed0);

      //Save it for next time
      timeSpeed0 = timeSpeed1;
    }
  }
  //Fuel Sensor
  if (digitalRead(DI_FUEL) == HIGH)
  {
    //Serial.println("HIGH");
    
    if (fuelAlreadyHIGH == false) //Transitioned from LOW to HIGH, start counter
    {
      fuelStartT = millis();
      fuelAlreadyHIGH = true;
    }
  }
  else //LOW
  {
    //Serial.println("LOW");
    //Transitioned from HIGH to LOW, end counter
    if (fuelAlreadyHIGH == true) 
    {
      //get next counter
      fuelEndT = millis();
      fuelAlreadyHIGH = false;
      fuelFreq = (1000/((fuelEndT - fuelStartT)/FUEL_DUTY_CYCLE)); //f=1/T, 1s = 1000ms
      //Serial.print("Current fuel: ");
      //Serial.println(fuelFreq, DEC);
      
      //INJECTOR mL * CYLINDERS * 1/SECOND = TOTAL mL PER SECOND
      //mL/s / m/s = mL/m = L/km
      //L/km /100 = L/100km
      fuelCurrentVal = (INJECTOR_SIZE*CYLINDER_COUNT*fuelFreq)/(100*speedCurrentVal); 

      //Calculate a rolling average
      fuelAverageVal = (fuelAverageVal + fuelCurrentVal)/2;
    }
  }
}

void resetEverything()
{
  #ifdef _DEBUGMODE
    Serial.println("Reset Everything!");
  #endif
  //Reset everything
  messageStatus = MESSAGE_RX_IDLE;
  calculateChecksum = true;
  charCount = 0;
  myChecksum = 0;
  memset(rxMessageBuffer, 0, sizeof(rxMessageBuffer));//Clear RX Buffer
  memset(messageChecksum, 0, sizeof(messageChecksum));//Clear Checksum Buffer
  memset(txMessageBuffer, 0, sizeof(txMessageBuffer));//Clear TX Buffer
}

void processMessage()
{
  //Process the rest of the message
  for (int i=0; i<= SP_MAX_PARAMS-1; i++)
  {
    #ifdef _DEBUGMODE
      Serial.println(i);
    #endif
    //Get next index
    sptr = strtok(NULL, DELIM_PARAM);
    if (sptr == NULL)
    {
      break;
    }
    strcpy(rxMessageParamter[i], sptr);
    #ifdef _DEBUGMODE
      Serial.println(rxMessageParamter[i]);
    #endif
      
  }
}

void sendMessage(char *txMessageType, char *txMessageParams)
{

  #ifdef _DEBUGMODE
    Serial.println("Sending Message!");
  #endif
  //Check lengths
  if (strlen(txMessageParams) > PAYLOAD_MAX_LENGTH)
  {
    sysError = PAYLOAD_EXCEEDS_LENGTH;
    return;
  }
  
  if (strlen(txMessageType) > SP_TYPE_LENGTH)
  {
    sysError = TYPE_EXCEEDS_LENGTH;
    return;
  }
    
  //Initialise message
  char payloadMessage[BUFFER_SIZE]; //Size of type + delim + payload
  char txMessage[BUFFER_SIZE + SP_CHECKSUM_LENGTH + 3]; //Size of START + type + delim + payload + delim + checksum

  //Create string to calculate checksum
  sprintf (payloadMessage, "%s%s%s", txMessageType, DELIM_PARAM, txMessageParams);
  
  //Calculate checksum
  int txChecksum = 0;
  for (int i=0; i<=strlen(payloadMessage); i++)
  {
    txChecksum ^= payloadMessage[i];
  }

  //Formulate message to send
  sprintf (txMessage, "%c%s%c%x%c", START, payloadMessage, DELIM_CHECKSUM, txChecksum, ESCAPE);

  //Send Message
  //mySerial
  mySerial.print(txMessage);
}