# ELE3305 - Computer Systems and Communications Protocols
## **Design Assignment**

## PLEASE NOTE: THIS REPORT VERSION HAS BEEN DEPRECATED AND REPLACED BY THE PDF GENERATED USING MICROSOFT WORD.  THIS FILE IS CONSIDERED A DRAFT VERSION OF THE FINAL REPORT, LEFT JUST FOR THE PURPOSE OF INFORMING THE MARKER THAT WE MADE THE EFFORT TO LEARN HOW TO USE MARKDOWN.

### Team R - Brett Francis, Coenraad Wium, Sam Stringer
---
## Introduction
This report will give an analysis of the protocols, coding, functionality, and conceptualisation stages for ELE3305 Assignment 3 requirements given to team R under the official company SeverTech Ltd. During the collaborative project, implementation of requirements by USQ comprised of a developed system to accurately give distance, speed and fuel telemetry for the purpose of the design constraints within Assignment 3. The team members consisting of Brett Francis, Sam Stringer and Coenraad Wium were responsible for generation of code, error checking, concept and team development, protocol implementation, report discussion, collaborative meetings, and operational analysis. This report is the progression of the project from conceptualisation through to commissioning and testing using multiple software platforms.

## Problem Statment
The system is a car monitoring system. It updates fuel, speed and distance every 250 millisecond but only displays them every second. Further details of how these microprocessors are connected are included in the system functional specifications details that follow. Four distance sensors from the distance sensor units are calibrated to measure distance and minimum value over all four. Fuel and speed sensor unit calculate fuel consumption, average fuel use, speed and distance driven. 

## Over View of System

This design project utilises the RS232 Serial communication interface to allow communication on the application layer. It will be using the available data from the parking sensors, speed reference and rpm reference to provide telemetry on distance location for rear sensors as well as speed feedback and fuel consumption based off a standard four-cylinder vehicle.

![alt text](./images/Figure1.png)

The system displayed in Figure 1 is a hardware depiction of the telemetry provided within the system. The four separate distance sensors give a raw feedback to the Arduino [4] Slave 2 used in calculation of the distance minimum value to the Slave 2 system and the position of each distance echo unit for use with the display and alarm within the Master Arduino. The speed reference is used for calculation of time-based reference for a kilometre per hour feedback and the rpm reference is used to calculate the fuel usage based of an assumed wheel diameter and valve basis of compression to turn ratio. The speed and fuel usage data are calculated within the slave one unit, before being transmitted on request from the master. The Slave one and two units transmit their data through a 74HC08 AND gate chip for value high transmission into the Master Arduino.

The communication system being utilised is the National Marine Electronics Association (NMEA) protocol on the RS232 Serial communication channels [1][2][3]. This allows for information to be dispersed and collected in the packet header with polling and collection protocols utilising NMEA’s own checksum arrangement for data correction and accuracy.
This system of communication, parsing and feedback for the frequency oscillations representing the rpm and speed sensors, as well as the pulsed distance sensors is done on TinkerCAD for the given arrangement designated to Team R. The TinkerCAD arrangement, shown on Figure 2 below, shows the connection of the devices, how they are interconnected and their given pin out arrangement.

![alt text](./images/Figure2.png)

The combination of NMEA protocol, RS232 communication, Arduino Code and TinkerCAD will be used to form a competent display of the requested outputs for speed, fuel use, distance, and verbal warning buzzer.


## Protocol functional specification
A protocol has been well specified and design decisions are well referenced.

The protocol required is to be an Application Layer protocol, operating over RS-232 serial.  The protocol must have the following requirements:
- Able to transmit several types of data
- Simple shared medium access
- Addressing
- Error checking

RS-232 is 
-more information on RS-232

The protocol to be developed is a simple ASCII protocol based.  After some deliberation, it was decided that the protocol was to be based on an existing protocol, NMEA 0183, defined and controlled by National Marine Electronics Association (NMEA).  The protocol is a simple ASCII based application protocol, over a RS-232 or EIA-422 serial interface.  The protocol has several desirable characteristics.  First, all data is transmitted as a printable ASCII character, which has the advantage of being human-readable.  All messages have start and escape character, a message identifier, several comma delimited fields, and a checksum.  A sample sentace (a message sent) is shown below:

- Sample message

- Analyse message
In the case of NMEA 0183, the start deliminator is      

- Protocol Terms
Application Message	The full text string that is sent over the serial port.  Consists of the message type, payload and checksum.
Start Character		Character that indicates the start of the application message.  
Escape Character	Character that identifies the end of the application message.
Delimitator		Character that separates the components of the application message.
Message Type	A component of the application message.  The string that identifies the talker and the message being sent.
Payload	A component of the application message.  Consists of data entries separated by the payload delimitator.
Data Entries	Individual data blocks that make up the payload.
Checksum	Checksum of all characters in the payload. 
Max Message Length	The maximum length of the application message.

- design decisions referencing????

--- 
Break down into each of the three components. Display, fuel/speed, distance.
## System functional specification
Accurately outlines the program objectives. Includes collation and analysis of data required for the programming and is well presented.
- Requirement statements


## Fuel and Speed Sensor Unit
Is required to convert signals from the ECU into speed and fuel consumption.
Speed and fuel have similar structure when being calculated. As seen in image one, the square waveform was assumed to be the revolution from the crank’s perspective for fuel and one revolution of the wheels for speed. Assuming a 4-stroke engine, the stages of combustion are intake, compression, combustion and exhaust. The assumptions are made that this is a 4-cylinder engine and that every cycle will represent the entire combustion cycle of all four cylinders. From image one, the decision was made to take the value from leading edge to falling edge; this would represent the period T.

![alt text](./images/Speed1.png)
### I/O
#define COM_RX    2\
#define COM_TX    3\
#define COM_EN    11\
#define DI_FUEL   5\
#define DI_SPEED  6\
#define DO_LED    13
### Internal Calculations
Additional assumptions made are the wheel circumference of 1994mm, injector size of 200 cc/min and has a duty cycle is assumed to be 50%.

### Fuel Calculation
Fuel Frequency = 1000 ÷ ((End Time - Start Time))/(Duty Cycle)\
Speed Value = Injector Size × Cylinder Count×Fuel Frequency

- Average Fuel Consumption

### Speed Calculation
Speed Frequency = 1000 ÷ ((End Time - Start Time))/(Duty Cycle)\
Speed Value = Wheel Circumference × Speed Frequency

- Distance Drive

## Distance Sensor Unit
The four Parallax PING))) ultrasonic distance sensor accuracy is from 2 cm to 3 meters(PING))); it operates by transmitting ultrasonic bursts the equates to a time for that burst to return to the sensor. Commonly called chirp and echo to indicate their function as hardware. Measuring the echoes pulse width gives the distance to the target when calculated. Image two shows the chirp (tout) and delay of the echo.

![alt text](./images/DistanceSensor1.png)
### I/O
#define COM_RX      2\
#define COM_TX      3\
#define COM_EN      11\
#define DIO_SENSOR0 5\
#define DIO_SENSOR1 6\
#define DIO_SENSOR2 7\
#define DIO_SENSOR3 8\
#define DO_LED      13
### Internal Calculations
Timings for the ultrasonic distance sensor operation are given in image three (Ultrasonic Distance Sensor - Parallax, 2021). For this design, timings are adhered to in accordance with image three; this accounts for correct operation.

![alt text](./images/DistanceSensor2.png)
## Limitations of PING))) Distance Sensor System
- The obstacle is more than 3 meters away or closer than 2 cm
- The surface has a shallow angle and isn’t reflecting back to the unit
- The item is too small

Factors that affect the speed of sound also affect the reliability of the unit. Air temperature can cause errors of 11 - 12%; conversion constants are used to account for the air temperature and can be incorporated into this assignment if an additional temperature sensor is used.

C = 331.5 (0.6 × T_c ) m/s

The speed of sound in 20-degree dry air is 343 m/s. This estimation has been used as a baseline.

## Display Unit
Display values from sensors to the driver, delivers audible alarms.
### I/O
#define COM_RX    2\
#define COM_TX    3\
#define COM_EN    11\
#define KCD_RS    8\
#define LCD_EN    9\
#define LCD_D5    5\
#define LCD_D6    6\
#define LCD_D7    7\
#define DO_SOUND  10\
#define DO_LED    13

## Program conceptualisation and documentation
The description of the program operation is logically structured and well written. It contains all relevant details and is of a suitable length.
- flow charts of program, diamonds and circles 
- AKA program description as seen in ELE1301 page 8

## Program code
The structure of the program and the interfaces are suitable and demonstrate highly effective code.

`code`

## Program operation
The program operated as specified.
Method of testing, some sort of table/simulator output
- individually 
- together

## References
1.	SiRF Technology Inc. December, 2007, NMEA Reference Manual, San Jose, CA. https://www.sparkfun.com/datasheets/GPS/NMEA%20Reference%20Manual-Rev2.1-Dec07.pdf
2.	National Marine Electronics Association. Jan, 2002. NMEA 0183 – Standard For Interfacing Marine Electronic Devices. Ver.3.01. https://www.plaisance-pratique.com/IMG/pdf/NMEA0183-2.pdf 
3.	CT Communication Technology. 2021. “NMEA Input/Output Messages 12 KANAL GPS (SIRF CHIPSET)”.  http://www.scarpaz.com/Attic/Documents/GPS-NMEA.pdf
4.	Ardunio. 2021. https://www.arduino.cc 
5.	TinkerCAD. 2021. https://www.tinkercad.com/
6. Parallax. 2021. PING))) Ultrasonic Distance Sensor - Parallax. [online] Available at: <https://www.parallax.com/product/ping-ultrasonic-distance-sensor/> [Accessed 23 May 2021].

---
### Bold

**bold text**

### Italic

*italicized text*

### Blockquote

> blockquote

### Ordered List

1. First item
2. Second item
3. Third item

### Unordered List

- First item
- Second item
- Third item

### Code

`code`

### Horizontal Rule

---

### Link

[title](https://www.example.com)

### Image

<<img src="../images/image.jpg" alt="its an image"/>

![alt text](../images/image.jpg)

## Extended Syntax

These elements extend the basic syntax by adding additional features. Not all Markdown applications support these elements.

### Table

| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |

### Fenced Code Block

```
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

### Footnote

Here's a sentence with a footnote. [^1]

[^1]: This is the footnote.

### Heading ID

### My Great Heading {#custom-id}

### Definition List

term
: definition

### Strikethrough

~~The world is flat.~~

### Task List

- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media
