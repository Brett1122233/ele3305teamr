//Include Libraries
#include <SoftwareSerial.h>
#include <LiquidCrystal.h>
#include <String.h>

//Simulating in Tinkercad?
//#define _tinkercad

//Display debug messages?
#define _DEBUGMODE

//Pin Assignments
#define COM_RX     2 
#define COM_TX     3

#define LCD_RS     8  
#define LCD_EN     9
#define LCD_D4     4
#define LCD_D5     5
#define LCD_D6     6
#define LCD_D7     7

#define DO_SOUND  10 
#define DO_LED      13

//Define Message
#define slaveAddress "DDRQ"

// Protocol Constants
#define START 0x24 //Start of application message, <$>
#ifdef _tinkercad //different for tinkercad simulation
  #define ESCAPE 0x25  //End of application message, <%>
#else
  #define ESCAPE 0x0D  //End of application message, <CR>
#endif
#define DELIM_PARAM "," //Delimiter of payload parameters <,>
#define DELIM_CHECKSUM 0x2A //Delimiter payload and checksum <*>

// Protocol Specfic Length
#define PAYLOAD_MAX_LENGTH 100 //100 bytes (100 characters in payload, 110 total)
#define PARAM_MAX_LENGTH 15 //bytes
#define SP_TYPE_LENGTH 4 //Application message type length
#define SP_CHECKSUM_LENGTH 2 //Checksum length
#define BUFFER_SIZE (PAYLOAD_MAX_LENGTH+SP_TYPE_LENGTH+2) //Size of payload + type + delim + null termination
#define SP_MAX_PARAMS 10 //Max number of paramters

//Message RX Status
#define MESSAGE_RX_IDLE 0
#define MESSAGE_RX_INPROGRESS 1
#define MESSAGE_RX_READY 2

// Protocol Status Constants
#define SUCCESS 0
#define NO_ERROR 0
#define CHECKSUM_FAILURE 1
#define PAYLOAD_EXCEEDS_LENGTH 2
#define TYPE_EXCEEDS_LENGTH 3
#define START_BYTE_UNEXPECTED 4
#define UNKNOWN_MESSAGE_TYPE 5

// Display Modes
#define DISPLAY_MODE_SPEED 0
#define DISPLAY_MODE_DISTANCE 1

//Display Screens
#define DISPLAY_SPEED_DIST 0
#define DISPLAY_ECON 1
#define DISPLAY_DIST_SEN 0

//Update Period
#define UPDATE_PERIOD 250
#define UPDATE_SCREEN 4 //multiples of UPDATE_PERIOD

//Math
#define MPS2KMH 3.6 //Metres per second to KM/h multiplier

//Tone
#define TONE_FREQ 700 //Hertz

#define TONE_LEVEL_OFF 0
#define TONE_LEVEL_FAR 1
#define TONE_LEVEL_MED 2
#define TONE_LEVEL_CLOSE 3

#define TONE_THRESHOLD_FAR 2 //m
#define TONE_THRESHOLD_MED 1 //m
#define TONE_THRESHOLD_CLOSE 0.5 //m

//PRECISION
#define FLOAT_DPOINTS 2 //Digits after decimal point float.

//Start Serial Port
SoftwareSerial mySerial(COM_RX, COM_TX);

//Start LCD
LiquidCrystal lcd(LCD_RS,LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);

//Internal Variables
//Serial Port RX/TX
char rxMessageBuffer[BUFFER_SIZE]; //Buffer for rx recieved
char messageChecksum[SP_CHECKSUM_LENGTH+1]; //Plus string terminator
int messageStatus = MESSAGE_RX_IDLE;
int charCount = 0;
bool calculateChecksum = true;
int myChecksum = 0;
int byteRead = 0;
int sysError = NO_ERROR;
char rxMessageParamter[SP_MAX_PARAMS][PARAM_MAX_LENGTH]; //Parameters
char rxMessageType[SP_TYPE_LENGTH+2];
char *sptr;

//Update Counters
long updateTimer0;
long updateTimer1;
int updatecount = 0;

//Display Mode and Screen
int displayMode = DISPLAY_MODE_SPEED;
int currentScreen = DISPLAY_SPEED_DIST;

//Tone
int tonelevel = TONE_LEVEL_OFF;

//Extra Buffers
char lcdLine1[17]; //LCD Line 1
char lcdLine2[17]; //LCD Line 2
char sfuelCurrentVal[15];
char sfuelAverageVal[15];
char sspeedCurrentVal[15];
char sdistance0[15];
char sdistance1[15];
char sdistance2[15];
char sdistance3[15];
char sdistanceMin[15];

//Initialise Sensor Values
//Distance Sensors
float distance0 = 0; //mm
float distance1 = 0; //mm
float distance2 = 0; //mm
float distance3 = 0; //mm
float distanceMin = 0; //mm

//Current Speed
float speedCurrentVal = 0; //METRES PER SECOND

//Current Distance travelled
int distanceCurrentVal = 0; //km

//Fuel Economy
float fuelCurrentVal = 0; //L PER 100km
float fuelAverageVal = 0; //L PER 100km

void setup() {
  //Start Serial for Debugging
  #ifdef _DEBUGMODE
    Serial.begin(9600);
    Serial.println("Starting..");
  #endif

  //Start Serial Communication to slaves
  mySerial.begin(2400);
  
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  //start update timer
  updateTimer0 = millis();
  
  //Update strings
  if(fuelCurrentVal < 90)
  {
    dtostrf(fuelCurrentVal, 0, FLOAT_DPOINTS, sfuelCurrentVal);
  }
  else
  {
    strcpy(sfuelCurrentVal, "inf");
  }
  dtostrf(fuelAverageVal, 0, FLOAT_DPOINTS, sfuelAverageVal);
  dtostrf(speedCurrentVal*MPS2KMH, 0, FLOAT_DPOINTS, sspeedCurrentVal);

  //Set LED to be an output
  pinMode (DO_LED, OUTPUT);
}

void loop() {
  //Check serial port for data
  //myserial
  if (Serial.available() > 0 && sysError <= NO_ERROR && (messageStatus == MESSAGE_RX_IDLE || messageStatus == MESSAGE_RX_INPROGRESS))
  {
    //read byte
    //myserial
    byteRead = Serial.read();

    //Make sure we're not too long
    if (charCount > BUFFER_SIZE)
    {
      sysError = PAYLOAD_EXCEEDS_LENGTH;
    }
    else
    {
      //check if we're already recieving a message
      if (messageStatus == MESSAGE_RX_INPROGRESS)
      {
        switch(byteRead)
        {
          case START: //Unexpected START char in message
            sysError = START_BYTE_UNEXPECTED;
            break;
          case ESCAPE: //End of Transmission
            //Check checksum is matches what has been calculated
            #ifdef _DEBUGMODE
              Serial.println(myChecksum);
              Serial.println(strtol(messageChecksum, 0, 16));
            #endif
            //Convert string to hex (base 16)
            if (strtol(messageChecksum, 0, 16) == myChecksum)
            {
              messageStatus = MESSAGE_RX_READY;
              sysError = SUCCESS;
            }
            else
            {
              sysError = CHECKSUM_FAILURE;
            }
            break;
          case DELIM_CHECKSUM: //Checksum is getting transmitted
            calculateChecksum = false;
            //convert char array to string
            rxMessageBuffer[charCount] = '\0';
            //Reset char count
            charCount = 0;
            break;
          default:
            if (calculateChecksum  == true)
            {
              myChecksum ^= byteRead; //XOR
              rxMessageBuffer[charCount] = byteRead;
            }
            else
            {
              //Recieving Checksum
              messageChecksum[charCount] = byteRead;
            }
            charCount++;
        }
      }
      else //Not receiving a message currently, first character!
      {
        resetEverything();

        //Make sure we are receiving the START character
        if (byteRead == START)
        {
          messageStatus = MESSAGE_RX_INPROGRESS;
        }//else ignore it!
      }
    }
  }

  //Check if we've encountered an RX Error
  if (sysError > NO_ERROR)
  {
    #ifdef _DEBUGMODE
      switch(sysError)
      {
        case UNKNOWN_MESSAGE_TYPE:
          Serial.println("Unknown Message Type");
          break;
        case CHECKSUM_FAILURE:
          Serial.println("Checksum Fail");
          break;
        case PAYLOAD_EXCEEDS_LENGTH:
          Serial.println("Message too long");
          break;
        case TYPE_EXCEEDS_LENGTH:
          Serial.println("Payload too long");
          break;
        case START_BYTE_UNEXPECTED:
          Serial.println("Start byte recieved unexpectedly");
          break;
        default:
          Serial.println("Unknown error!");
          break;      
      }
    #endif
    sysError = NO_ERROR;
    resetEverything();
  }

  //Parse recieved message
  if(messageStatus == MESSAGE_RX_READY)
  {
    #ifdef _DEBUGMODE
      Serial.println("String RX OK.");
      Serial.print("Recieved ");
      Serial.print(strlen(rxMessageBuffer));
      Serial.println(" chars.");
    #endif
    
    //Isolate the first part
    sptr = strtok(rxMessageBuffer, DELIM_PARAM);

    //Move it to the type
    strcpy(rxMessageType, sptr);
    
    #ifdef _DEBUGMODE
      Serial.print("Message type '");
      Serial.print(rxMessageType);
      Serial.println("'");
    #endif
    
    //Check if we can handle this message
    //Rear Distance Sensor
    if (strcmp(rxMessageType, "SSRD") == 0)
    {
      #ifdef _DEBUGMODE
        Serial.println("Message is able to be handled");
      #endif
      //get values
      processMessage();

      //Convert values from mm to m
      //mm * 1m/1000mm = m
      distance0 = atoi(rxMessageParamter[0])/1000.00;//m
      distance1 = atoi(rxMessageParamter[1])/1000.00;//m
      distance2 = atoi(rxMessageParamter[2])/1000.00;//m
      distance3 = atoi(rxMessageParamter[3])/1000.00;//m
      distanceMin = atoi(rxMessageParamter[4])/1000.00;//m
      
      //convert to string
      dtostrf(distance0, 0, FLOAT_DPOINTS, sdistance0);
      dtostrf(distance0, 0, FLOAT_DPOINTS, sdistance1);
      dtostrf(distance0, 0, FLOAT_DPOINTS, sdistance2);
      dtostrf(distance0, 0, FLOAT_DPOINTS, sdistance3);
      dtostrf(distanceMin, 0, FLOAT_DPOINTS, sdistanceMin);

      //check threshold for distance
      if(distanceMin <= TONE_THRESHOLD_CLOSE)
      {
        tonelevel = TONE_LEVEL_CLOSE;
        displayMode = DISPLAY_MODE_DISTANCE;
      }
      else if(distanceMin <= TONE_THRESHOLD_MED)
      {
        tonelevel = TONE_LEVEL_MED;
        displayMode = DISPLAY_MODE_DISTANCE;
      }
      else if(distanceMin <= TONE_THRESHOLD_FAR)
      {
        tonelevel = TONE_LEVEL_FAR;
        displayMode = DISPLAY_MODE_DISTANCE;
      }
      else
      {
        tonelevel = TONE_LEVEL_OFF;
        displayMode = DISPLAY_MODE_SPEED;
      }
      
      //Request Speed/Fuel message
      sendMessage(slaveAddress, "SSSF");
    }
    else if (strcmp(rxMessageType, "SSSF") == 0)
    {
      #ifdef _DEBUGMODE
        Serial.println("Message is able to be handled");
      #endif
      //get values
      processMessage();

      
      //convert from string
      fuelCurrentVal = atof(rxMessageParamter[0]);
      fuelAverageVal = atof(rxMessageParamter[1]);
      speedCurrentVal = atof(rxMessageParamter[2]);
      distanceCurrentVal = atoi(rxMessageParamter[3]);

      //Make string
      dtostrf(fuelCurrentVal, 0, FLOAT_DPOINTS, sfuelCurrentVal);
      dtostrf(fuelAverageVal, 0, FLOAT_DPOINTS, sfuelAverageVal);
      dtostrf(speedCurrentVal, 0, FLOAT_DPOINTS, sspeedCurrentVal);

      //Request distance message
      sendMessage(slaveAddress, "SSRD");
    }
    else {
      #ifdef _DEBUGMODE
        Serial.println("Message is unknown");
      #endif
      sysError = UNKNOWN_MESSAGE_TYPE;
    }
    resetEverything();
  }
  
  //Check if it's time to update the display
  updateTimer1 = millis();
  if ((updateTimer1 - updateTimer0) > UPDATE_PERIOD)
  {
    //reset timer
    updateTimer0 = updateTimer1;
    
    //Update counters
    updatecount ++;
    
    //What mode are we in?
    switch (displayMode)
    {
      case DISPLAY_MODE_SPEED:
        if (updatecount > UPDATE_SCREEN)
      {
          updatecount = 0;
          currentScreen++;
          //What screen is being displayed?
          switch(currentScreen)
          {
            case DISPLAY_ECON:
              //Whitespace overwrites what's on the LCD
              //16 chars limited
              snprintf (lcdLine1, 16, "CE %s L/100km       ", sfuelCurrentVal);
              snprintf (lcdLine2, 16, "AE %s L/100km       ", sfuelAverageVal);
              break;
            default: //DISPLAY_SPEED_DIST:
              snprintf (lcdLine1, 16, "%s Km/h       ", sspeedCurrentVal);
              snprintf (lcdLine2, 16, "ODO %u km        ", distanceCurrentVal);
              //reset screen count
              currentScreen = 0;
          }
        }
        break;
      case DISPLAY_MODE_DISTANCE:
        switch (updatecount)
        {
          case 1:
            switch (tonelevel)
            {
              case TONE_LEVEL_FAR:
              case TONE_LEVEL_MED:
                noTone(DO_SOUND); 
                digitalWrite(DO_LED, LOW);
          break;
              case TONE_LEVEL_CLOSE:
                tone(DO_SOUND,TONE_FREQ); 
                digitalWrite(DO_LED, HIGH);
          break;
              default:
                tonelevel = TONE_LEVEL_OFF;
                noTone(DO_SOUND);
                digitalWrite(DO_LED, LOW);
            }
            break;
          case 2:
            switch (tonelevel)
            {
              case TONE_LEVEL_FAR:
                noTone(DO_SOUND); 
                digitalWrite(DO_LED, LOW);
          break;
              case TONE_LEVEL_MED:
              case TONE_LEVEL_CLOSE:
                tone(DO_SOUND,TONE_FREQ); 
                digitalWrite(DO_LED, HIGH);
          break;
              default:
                tonelevel = TONE_LEVEL_OFF;
                noTone(DO_SOUND);
                digitalWrite(DO_LED, LOW);
            }
            break;
          case 3:
            switch (tonelevel)
            {
              case TONE_LEVEL_FAR:
              case TONE_LEVEL_MED:
                noTone(DO_SOUND); 
                digitalWrite(DO_LED, LOW);
          break;
              case TONE_LEVEL_CLOSE:
                tone(DO_SOUND,TONE_FREQ); 
                digitalWrite(DO_LED, HIGH);
          break;
              default:
                tonelevel = TONE_LEVEL_OFF;
                noTone(DO_SOUND);
                digitalWrite(DO_LED, LOW);
            }
            break;
          default:
            switch (tonelevel)
            {
              case TONE_LEVEL_FAR:
              case TONE_LEVEL_MED:
              case TONE_LEVEL_CLOSE:
                tone(DO_SOUND,TONE_FREQ); 
                digitalWrite(DO_LED, HIGH);
          break;
              default:
                tonelevel = TONE_LEVEL_OFF;
                noTone(DO_SOUND);
                digitalWrite(DO_LED, LOW);
            }
            updatecount = 0;
            snprintf (lcdLine1, 16, "Min: %i cm       ", distanceMin);
            snprintf (lcdLine2, 16, "%i %i %i %i", distance0, distance1, distance2, distance3); 
        }
        break;
      default:
        //Shouldn't happen, back to known state.
        displayMode = DISPLAY_MODE_SPEED;
    }
      
    //Return to 0,0
    lcd.home();
    //Print first line
    lcd.print(lcdLine1);
    //Print second line
    lcd.setCursor(0, 1);
    lcd.print(lcdLine2);
  }
}

void resetEverything()
{
  #ifdef _DEBUGMODE
    Serial.println("Reset Everything!");
  #endif
  //Reset everything
  messageStatus = MESSAGE_RX_IDLE;
  calculateChecksum = true;
  charCount = 0;
  myChecksum = 0;
  memset(rxMessageBuffer, 0, sizeof(rxMessageBuffer));//Clear RX Buffer
  memset(messageChecksum, 0, sizeof(messageChecksum));//Clear Checksum Buffer
}

void processMessage()
{
  //Process the rest of the message
  for (int i=0; i<= SP_MAX_PARAMS-1; i++)
  {
    #ifdef _DEBUGMODE
      Serial.println(i);
    #endif
    //Get next index
    sptr = strtok(NULL, DELIM_PARAM);
    if (sptr == NULL)
    {
      break;
    }
    strcpy(rxMessageParamter[i], sptr);
    #ifdef _DEBUGMODE
      Serial.println(rxMessageParamter[i]);
    #endif
      
  }
}

void sendMessage(char *txMessageType, char *txMessageParams)
{

  #ifdef _DEBUGMODE
    Serial.println("Sending Message!");
  #endif
  //Check lengths
  if (strlen(txMessageParams) > PAYLOAD_MAX_LENGTH)
  {
    sysError = PAYLOAD_EXCEEDS_LENGTH;
    return;
  }
  
  if (strlen(txMessageType) > SP_TYPE_LENGTH)
  {
    sysError = TYPE_EXCEEDS_LENGTH;
    return;
  }
    
  //Initialise message
  char payloadMessage[BUFFER_SIZE]; //Size of type + delim + payload
  char txMessage[BUFFER_SIZE + SP_CHECKSUM_LENGTH + 3]; //Size of START + type + delim + payload + delim + checksum

  //Create string to calculate checksum
  sprintf (payloadMessage, "%s%s%s", txMessageType, DELIM_PARAM, txMessageParams);
  
  //Calculate checksum
  int txChecksum = 0;
  for (int i=0; i<=strlen(payloadMessage); i++)
  {
    txChecksum ^= payloadMessage[i];
  }

  //Formulate message to send
  sprintf (txMessage, "%c%s%c%x%c", START, payloadMessage, DELIM_CHECKSUM, txChecksum, ESCAPE);

  //Send Message
  //mySerial
  Serial.print(txMessage);
}
